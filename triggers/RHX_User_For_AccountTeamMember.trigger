trigger RHX_User_For_AccountTeamMember on User  (after insert, after update, after delete, after undelete) {
    if (trigger.isAfter) {
        Type rollClass = System.Type.forName('rh2', 'ParentUtil');
        if(rollClass != null) {
            rh2.ParentUtil pu = (rh2.ParentUtil) rollClass.newInstance();
            Database.update(pu.performTriggerRollups(trigger.oldMap, trigger.newMap,  'AccountTeamMember', null), false);
        }         				
    }
}