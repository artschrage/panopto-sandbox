trigger CustomQuoteLineTrigger on SBQQ__QuoteLine__c (before insert, before update) {
	if (Trigger.isBefore) {
		for (SBQQ__QuoteLine__c line : Trigger.new) {
			line.Compound_Discounted_Price__c = line.SBQQ__ProratedPrice__c;
			if ((line.SBQQ__CompoundDiscountRate__c != null) && (line.SBQQ__ProratedPrice__c != null)) {
				Decimal qty = (line.SBQQ__Quantity__c != null) ? line.SBQQ__Quantity__c : 0;
				line.Compound_Discounted_Price__c =  line.SBQQ__ProratedPrice__c * (1.0 / Math.pow(qty.doubleValue(), (line.SBQQ__CompoundDiscountRate__c / 100.0).doubleValue()));  
			}
		}
	}
}