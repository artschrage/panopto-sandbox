trigger AccountBeforeUpsert on Account (before insert, before update) {
	
	// Set the Region__c and Territory__c fields based on the BillingCountry
	AccountRegionTerritory art = new AccountRegionTerritory();
	art.setRegionTerritory(Trigger.isInsert, Trigger.new, Trigger.oldMap);

}