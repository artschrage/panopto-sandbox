trigger assignSharedSecret on Account (before insert, before update) {

    for( Account a : Trigger.new ) {
        if( String.isBlank(a.Shared_Secret__c) ) {
            // Assign a valid shared secret using the GUID shaped random number generator.
            a.Shared_Secret__c = GuidShapedRandomNumberUtil.NewGuidShapedRandomNumber();
        }
    }
}