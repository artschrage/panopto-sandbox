trigger setAuthorizedPOC on PanoptoContract__c (before insert, before update) {

    // If there is a POC assigned, then make that person an authorized POC.
    for( PanoptoContract__c pc : Trigger.new ) {
        if( pc.Primary_POC__c != null ) {
            Contact c = new Contact(Id = pc.Primary_POC__c);
            c.Authorized_POC__c = true;
            update c;
        }
    }

}