@isTest
private class SummaUtilTest {

	static testMethod void testMapToRecordType(){
		// Start the test
		Test.startTest();
		Map<String,RecordType> rNameMap=SummaUtil.mapToRecordType();
		Map<String,RecordType> rIdMap=SummaUtil.mapIdToRecordType(rNameMap);
		// Stop the test
		Test.stopTest();
	}
    
    static testMethod void testMapRtIdToRevision(){
		// Start the test
		Test.startTest();
		Map<String,RecordType> rNameMap=SummaUtil.mapToRecordType();
		Map<String,String> result = SummaUtil.mapRtIdToRevision(rNameMap);
		// Stop the test
		Test.stopTest();
    }
}