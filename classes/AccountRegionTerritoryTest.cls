@isTest
private class AccountRegionTerritoryTest
{
	@isTest
	static void itShould()
	{
		Map<String,RecordType> rNameMap = SummaUtil.mapToRecordType();
		// Create the Territories
		TestDataFactory.createGenericTerritories();
		// Start Test
		Test.startTest();
		// Test 1: Create a new Account with the BillingCountry set
		Account[] accts = TestDataFactory.createGenericAccts(rNameMap);		
		// Test 2: Change the Billing Country
		accts[0].BillingCountry = 'United Kingdom';
		// Stop Test
		Test.stopTest();
	}
}