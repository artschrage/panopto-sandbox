public with sharing class TestDataFactory {

	public static Account[] createGenericAccts(Map<String,RecordType> rNameMap)
	{
        // Create test Account
        Account[] accts = new Account[]{};
        accts.add(new Account(name='Small Corp'
        					, RecordTypeId=rNameMap.get('Account-Standard_Account_non_partner').Id
                            ,  BillingCountry='United States'
        					));
		insert accts;
		return accts;
	}

	public static Territory__c[] createGenericTerritories()
	{
		// Create test Territory
		Territory__c[] terrs = new Territory__c[]{};
		terrs.add(new Territory__c(
					Country__c = 'United States'
					, Country_Code__c = 'US'
					, Territory__c = 'US & Canada'
					, Region__c = 'Americas'
					));
		terrs.add(new Territory__c(
					Country__c = 'United Kingdom'
					, Country_Code__c = 'GB'
					, Territory__c = 'Europe'
					, Region__c = 'EMEA'
					));
		insert terrs;
		return terrs;
	}
/*
	public static Contact[] createGenericContacts(Map<String,RecordType> rNameMap, Account[] accts){
		// Create the first test Contact
		Contact[] contacts = new Contact[]{};
		for (Account a: accts){
			contacts.add(new Contact(
							RecordTypeId = rNameMap.get('Contact-xxxx').Id
							, FirstName = 'Test'
							, LastName = 'TestContact'
							));
		}
		insert contacts;
		return contacts;
	}

	public static Opportunity[] createGenericOpps(Map<String,RecordType> rNameMap, Account[] accts, Contact[] contacts){
        // Create test Opportunity
        Opportunity[] opptys = new Opportunity[]{};
        opptys.add(new Opportunity(
        						Name='Test Oppty'
        						, RecordTypeId=rNameMap.get('Opportunity-xxxx').Id
        						, AccountId=accts[0].Id
        						, StageName='Prospecting'
        						, CloseDate=Date.today()
        						));
        insert opptys;
        return opptys;
    }
*/
}