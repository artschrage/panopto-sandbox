@isTest
private class CustomTriggerTests {
    testMethod static void testQuoteLine() {
        Opportunity opp = new Opportunity(Name='Test',CloseDate=System.today(),StageName='Prospecting');
        insert opp;
        
        SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id, SBQQ__SubscriptionTerm__c=30);
        insert quote;
        
        SBQQ__QuoteLine__c line = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.Id,SBQQ__ProratedPrice__c=100,SBQQ__Quantity__c=5,SBQQ__CompoundDiscountRate__c=0.25);
        insert line;
    }
}