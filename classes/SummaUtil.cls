global class SummaUtil {

	public static Map<String,RecordType> mapToRecordType(){
        // Create map from a Record Type's Developer Name to the Record Type object
        // The of the map is: SObject+DevloperName
        // Example: to get Record Type Id of the Account Record Type called "Corp" (developer name)
        //		SummaUtil.mapToRecordType('Account-Corp').Id
        // Note: Code should only call this method once to fetch the entire map.
        //			Subsequent lookups should be to the local variable copy (to guard governor limits)
    	Map<String,RecordType> rNameMap=new Map<String,RecordType>();
        for ( RecordType r : [Select Id,SobjectType,DeveloperName From RecordType Where IsActive=true] ) {
            rNameMap.put(r.SobjectType + '-' + r.DeveloperName, r);
        }
        return rNameMap;
	}

	public static Map<String,RecordType> mapIdToRecordType(Map<String,RecordType> rNameMap){
		// Create a map from RecordTypeId to RecordType object using rNameMap as the starting point
		// No additional queries required
		Map<String,RecordType> rIdMap = new Map<String,RecordType>{};
		for (RecordType r: rNameMap.values()){
			rIdMap.put(r.Id, r);
		}
		return rIdMap;
	}

	public static Map<String,String> mapRtIdToRevision(Map<String,RecordType> rNameMap){
		// Provide a recordtype map like is provided by the mapToRecordType method above
		// This method will return a map from RecordTypeId to a string that is one of two
		// values: Initial or Revision
		Map<String,String> returnMap=new Map<String,String>{};
		for (String dn: rNameMap.keySet()){
			if (dn.contains('Initial')){
				returnMap.put(rNameMap.get(dn).Id,'Initial');
			}
			if (dn.contains('Revision')){
				returnMap.put(rNameMap.get(dn).Id,'Revision');
			}
		}
		return returnMap;
	}

}