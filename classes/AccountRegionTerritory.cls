public with sharing class AccountRegionTerritory {
	public AccountRegionTerritory() {
		
	}

	// Set the Region_Name__c and Territory__c fields based on the BillingCountry
	// Referenced by the AccountBeforeUpsert trigger
	public void setRegionTerritory(Boolean triggerIsInsert, Account[] triggerNew, Map<Id,Account> triggerOldMap){
		System.debug('*** triggerNew.size()-1: ' + triggerNew.size());
		// Filter down to applicable records
		Account[] filteredAccts = filterForSetRegionTerr(triggerIsInsert, triggerNew, triggerOldMap);
		if (filteredAccts.size() > 0){
			System.debug('*** filteredAccts.size(): ' + filteredAccts.size());
			// Update the fields on the selected Accounts
			updateRegionTerr(filteredAccts);
		}
	}

	// Filter the Trigger.new down to records with a BillingCountry and Territory_Override__c not checked
	private Account[] filterForSetRegionTerr(Boolean triggerIsInsert, Account[] triggerNew, Map<Id,Account> triggerOldMap){
		System.debug('*** triggerNew.size()-2: ' + triggerNew.size());
		Account[] returnAccts = new Account[]{};
		for (Account a: triggerNew){
			if (triggerIsInsert){
				if (!String.isBlank(a.BillingCountry)
					&& !a.Territory_Override__c
					){
					returnAccts.add(a);
				}
			} else {
				System.debug('*** a.Name: ' + a.Name);
				System.debug('*** triggerOldMap.get(a.Id).Name: ' + triggerOldMap.get(a.Id).Name);
				System.debug('*** a.BillingCountry: ' + a.BillingCountry);
				System.debug('*** triggerOldMap.get(a.Id).BillingCountry: ' + triggerOldMap.get(a.Id).BillingCountry);
				// NOTE: Looks like a SF bug. The values of 'a.BillingCountry' and 'triggerOldMap.get(a.Id).BillingCountry'
				//       always match (both contain the old value). Salesforce uses some kind of sophisticated multi-step
				//		 processing for this country picklist feature which apparently is not working
				//
				// Instead, will check if the Region or Territory is blank. If it is, we will fill them

				// We decided to make this assignment code for Account updates run every time (even when there are no record changes)
				// This way, if Territory__c records are changed, an admin just has to do an edit on all of the
				// Accounts to get the Territory and Region to update on the Account to reflect the Territory changes
				//if (a.BillingCountry != triggerOldMap.get(a.Id).Last_Billing_Country__c){
				if (!String.isBlank(a.BillingCountry)
					&& !a.Territory_Override__c){
					returnAccts.add(a);
				}
				//}
			}
		}
		return returnAccts;
	}

	// Update the Region_Name__c and Territory__c fields based on the BillingCountry
	private void updateRegionTerr(Account[] accts){
		// Create a map from the Country name to the Territory Result (TerrResult)
		Map<String,TerrResult> mapCountryToRegionTerr = createCountryToTerrMap(accts);
		for (Account a: accts){
			if (mapCountryToRegionTerr.containsKey(a.BillingCountry)){
				a.Region_Name__c = mapCountryToRegionTerr.get(a.BillingCountry).region;
				a.Territory__c = mapCountryToRegionTerr.get(a.BillingCountry).terr;
			}
		}
	}

	// Create a map from the Country name to the Territory Result (TerrResult)
	private Map<String,TerrResult> createCountryToTerrMap(Account[] accts){
		// Query the SF DB to get the relevant Territory__c records
		Territory__c[] terrs = getTerrs(accts);
		// Create a Map from the Country name to the Territory data (so we don't have to loop through the whole list for every Account)
		Map<String,TerrResult> returnMap = new Map<String,TerrResult>{};
		for (Territory__c t: terrs){
			returnMap.put(t.Country__c, new TerrResult(t.Country__c, t.Region__c, t.Territory__c));
		}
		System.debug('*** returnMap.size(): ' + returnMap.size());
		return returnMap;
	}

	// Query the SF DB to get the relevant Territory__c records
	private Territory__c[] getTerrs(Account[] accts){
		Territory__c[] returnTerrs = new Territory__c[]{};
		Set<String> countries = new Set<String>();
		// Get the list of countries
		for (Account a: accts){
			// Note: We already filtered the Accounts to remove any blank BillingCountry
			countries.add(a.BillingCountry);
		}
		// Query the DB
		if (countries.size() > 0){
			returnTerrs = [Select Country__c, Country_Code__c, Region__c, Territory__c From Territory__c Where Country__c IN: countries];
		}
		System.debug('*** returnTerrs.size(): ' + returnTerrs.size());
		return returnTerrs;
	}

	// Custom class to hold the data returned from the query of the Territory__c object
	// This probably is not needed. When refactoring, may choose to just use the query results of Territory__c[] instead
	private class TerrResult {
		public String country;
		public String region;
		public String terr;
		public TerrResult(String c, String r, String t){
			country = c;
			region = r;
			terr = t;
		}
	}

}