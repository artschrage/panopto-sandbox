@isTest
public class AccountCreationTestClass 
{ 

    public static testmethod void accountCreationTest() 
    {
        Account acc = new Account(Name= 'my test account', BillingCountry = 'United States', BillingState = 'Utah');
        insert acc;

        Account retAcc = [select id, shared_secret__c from Account where id=:acc.id][0];
        System.assert(retAcc.shared_secret__c != null); // New accounts should have a shared secret value.
        System.assert(retAcc.shared_secret__c != ''); // New accounts should have a shared secret value.
    }
}