@isTest(SeeAllData=true)
public class TEST_RHX {
	
    static testMethod void dsfs_DocuSign_Status() {
        
        List<sObject> sourceList = [SELECT Id
FROM dsfs__DocuSign_Status__c
                    			LIMIT 1];

        if(sourceList.size() == 0) {
            sourceList.add(
                	new dsfs__DocuSign_Status__c()
                );
        }
	Database.upsert(sourceList);        
    }
    
        static testMethod void Invoice() {
        
        List<sObject> sourceList = [SELECT Id
FROM Invoice__c
                    			LIMIT 1];

        if(sourceList.size() == 0) {
            sourceList.add(
                	new Invoice__c()
                );
        }
	Database.upsert(sourceList);        
    }
    
        static testMethod void PanoptoContract() {
        
        List<sObject> sourceList = [SELECT Id
FROM PanoptoContract__c
                    			LIMIT 1];

        if(sourceList.size() == 0) {
            sourceList.add(
                	new PanoptoContract__c()
                );
        }
	Database.upsert(sourceList);        
    }
    
    static testMethod void Due_Dilligence_Form() {
        
        List<sObject> sourceList = [SELECT Id
FROM Due_Dilligence_Form__c
                    			LIMIT 1];

        if(sourceList.size() == 0) {
            sourceList.add(
                	new Due_Dilligence_Form__c()
                );
        }
	Database.upsert(sourceList);        
    }

}