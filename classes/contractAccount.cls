public class contractAccount {

    String id; 
    PanoptoContract__c contract;
    String accountName;
    List<Contact> contacts = new List<Contact>{};
    Boolean success;
    String errorMessage = '';
            
    // Returns a list of Contacts associated with the account. Then populates the option list
    // in the new trial VisualForce page.
    // In Apex, having a getFoo function means you can reference a list of Foo in visualforce pages.
    public List<SelectOption> getContacts() {
        List<SelectOption> options = new List<SelectOption>();
        if (this.success)
        {
            for (Integer idx = 0; idx < this.contacts.size(); idx++)
            {
                options.add(new SelectOption(this.contacts[idx].Id, this.contacts[idx].Name));
            }
        }
        return options;
    }
    
    // Gets the account name to display on the VisualForce page.
    // In Apex, having a getFoo function means you can reference Foo in visualforce pages.
    public String getAccount() {
        return this.accountName;
    }
    
    // Gets the error message to display on the VisualForce page.
    // In Apex, having a getFoo function means you can reference Foo in visualforce pages.
    public String getErrorMessage() {
        return this.errorMessage;
    }

    // Gets the success value to determine whether to show the VisualForce form.
    // In Apex, having a getFoo function means you can reference Foo in visualforce pages.
    public Boolean getSuccess() {
        return this.success;
    }
    
    // Determine if this account is allowed a new trial contract.
    public Boolean accountCanHaveTrial(Id accountId)
    {
        Boolean trialAllowed = true;
        
        // Find trial contracts for this account. If they are not active, expired, or invited, we can add another trial.
        List<PanoptoContract__c> panoptoTrialContractsInValidStates = new List<PanoptoContract__c>([SELECT Name FROM PanoptoContract__c WHERE Account__c = :accountId and Type__c = 'Trial' and (Site_Status__c = 'Active' or Site_Status__c = 'Invited' or Site_Status__c = 'Expired' )]);
        if (panoptoTrialContractsInValidStates.size() > 0)
        {
            System.debug('Trials for account: ' + accountId + ' trials count: ' + panoptoTrialContractsInValidStates.size());
            trialAllowed = false;
        }
        return (trialAllowed);
    }
    
    // Controller for the new trial VisualForce page.
    public contractAccount(ApexPages.StandardController controller) {
        this.success= false;
        contacts = null;
        contract = (PanoptoContract__c )controller.getRecord();
        id = ApexPages.currentPage().getParameters().get('idc'); 
        
        // We require the Account ID to be passed in so that we don't have trials incorrectly 
        // created. This also allows us to populate the contact list.
        if(id != null)
        {
            contract.Account__c = id; 
            // Find accounts with the ID passed in. There should only ever be one. We only need the
            // name so we can print it later.
            List<Account> accounts = new List<Account>([SELECT Name FROM Account WHERE Id = :id]);

            if (accounts.size() > 0)
            {
                // The account name variable is access from the visualforce page.
                this.accountName = accounts[0].Name;

                // Verify the account can have a trial added to it.
                if (accountCanHaveTrial(contract.Account__c))
                {
    
                    // Find all contacts associated with the account
                    contacts = new List<Contact>([SELECT Id, FirstName, LastName, Name FROM Contact WHERE AccountId = :id]);
                
                    if (!contacts.isEmpty())
                    {
                        // We were able to find the account and contacts, so success.
                        this.success = true;

                        // Set some default values for the trial. default POC, from wizard checkbox, duration, and language.
                        contract.Primary_POC__c = contacts[0].Id;
                        contract.fromWizard__c = true;
                        contract.Trial_Duration__c = 30;
                        contract.Language__c = 'English';
                    }
                    else
                    {
                        errorMessage += 'This account does not have any contacts to email the trial to. Please contact the Sales Ops team. Account:' + this.accountName;
                    }
                }
                else
                {
                    errorMessage += 'The account already has a trial and cannot have a new trial applied. Please contact the Sales Ops team. Account: ' + this.accountName;
                }
            }
            else
            {
                errorMessage += 'This account cannot be found. Please contact the Sales Ops team. Account ID: ' + id;
            }
        }
        else
        {
            errorMessage += 'This account cannot be found. Please contact the Sales Ops team. Account ID: null';
        }
        
        if (!this.success)
        {
            System.debug('Cannot create a trial. ' + errorMessage);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, errorMessage));
        }
    }
}