@isTest
public class PanoptoContractCreationTestClass 
{ 

    public static testmethod void panoptoContractCreationTest() 
    {
        Account acc = new Account(Name= 'my test account', BillingCountry = 'United States', BillingState = 'Utah');
        insert acc;
        
        Contact c = new Contact(Account = acc, LastName = 'Jones', FirstName = 'Jane');
        insert c;
        
        PanoptoContract__c pc = new PanoptoContract__c(Account__c = acc.id, Primary_POC__c = c.id);
        insert pc;

        PanoptoContract__c retPc = [select id, Account__c, Primary_POC__c from PanoptoContract__c where id=:pc.id][0];
        Contact retC = [select id, Authorized_POC__c from Contact where id=:c.id][0];
        System.assert(retPc.Account__c != null);       // Should be associated with the account.
        System.assert(retPc.Primary_POC__c != null);   // Should have assigned a POC.
        System.assert(retC.Authorized_POC__c);         // Should be an authorized POC.
    }
}